<?php include('../_partials/top.php') ?>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>

<div class="container-fluid mt--6">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="table-responsive">
				<table class="table align-items-center table-flush table-striped table-hover" id="datatable">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Tanggal</th>
							<th>Nominal</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $nomor = 1; ?>
						<?php foreach ($datax as $user) : ?>
						<tr>
							<td><?php echo $nomor++ ?></td>
							<td><?php echo $user['nama'] ?></td>
							<td><?php echo $user['tanggal'] ?></td>
							<td><?php echo $user['nominal'] ?></td>
							<td>
								<!-- Single button -->
								<div class="btn-group pull-right">
									<div class="dropdown">
											<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="caret"></span>
											</button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a href="edit.php?id=<?= $user['id'] ?>" class="dropdown-item"> <i class="fa fa-edit"></i>Edit </a>
												<a href="delete.php?id=<?php echo $user['id'] ?>" class="dropdown-item" onclick="return confirm('Yakin hapus data ini?')"> <i class="fa fa-trash"></i>Hapus</a>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include('../_partials/bottom.php') ?>
