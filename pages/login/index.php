<?php
session_start();

// jika sudah login, alihkan ke halaman dasbor
if (isset($_SESSION['user'])) {
  header('Location: ../dashboard/index.php');
  exit();
}
?>

<?php include('../_partials/top-login.php') ?>

<!-- Navbar -->
<nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
	<div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
			<div class="navbar-collapse-header">
				<div class="row">
					<div class="col-6 collapse-brand">
						<a href="dashboard.html">
						</a>
					</div>
					<div class="col-6 collapse-close">
						<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
						</button>
					</div>
				</div>
			</div>
			<hr class="d-lg-none" />
			<ul class="navbar-nav align-items-lg-center ml-lg-auto">
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="https://www.facebook.com/creativetim" target="_blank" data-toggle="tooltip" data-original-title="Like us on Facebook">
						<i class="fab fa-facebook-square"></i>
						<span class="nav-link-inner--text d-lg-none">Facebook</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="https://www.instagram.com/creativetimofficial" target="_blank" data-toggle="tooltip" data-original-title="Follow us on Instagram">
						<i class="fab fa-instagram"></i>
						<span class="nav-link-inner--text d-lg-none">Instagram</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="https://twitter.com/creativetim" target="_blank" data-toggle="tooltip" data-original-title="Follow us on Twitter">
						<i class="fab fa-twitter-square"></i>
						<span class="nav-link-inner--text d-lg-none">Twitter</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="https://github.com/creativetimofficial" target="_blank" data-toggle="tooltip" data-original-title="Star us on Github">
						<i class="fab fa-github"></i>
						<span class="nav-link-inner--text d-lg-none">Github</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- Main content -->
<div class="main-content">
	<!-- Header -->
	<div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
		<div class="container">
			<div class="header-body text-center mb-7">
				<div class="row justify-content-center">
					<div class="col-xl-5 col-lg-6 col-md-8 px-5">
						<h1 class="text-white">Welcome!</h1>
						<p class="text-lead text-white">Use these awesome forms to login or create new account in your project for free.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="separator separator-bottom separator-skew zindex-100">
			<svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
				<polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
			</svg>
		</div>
	</div>
	<!-- Page content -->
	<div class="container mt--8 pb-5">
		<div class="row justify-content-center">
			<div class="col-lg-5 col-md-7">
				<div class="card bg-secondary border-0 mb-0">
					<div class="card-body px-lg-5 py-lg-5">
						<div class="text-center text-muted mb-4">
							<small>Sign in with credentials</small>
						</div>
						<form method="POST" action="../login/proses-login.php">
							<div class="form-group mb-3">
								<div class="input-group input-group-merge input-group-alternative">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ni ni-email-83"></i></span>
									</div>
									<input type="text" name="username_user" class="form-control" placeholder="Username" required>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group input-group-merge input-group-alternative">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
									</div>
									<input type="password" name="password_user" class="form-control" placeholder="Password" required>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary my-4">Sign in</button>
							</div>
						</form>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-6">
						<!-- <a href="#" class="text-light"><small>Forgot password?</small></a> -->
					</div>
					<div class="col-6 text-right">
						<a href="../signup" class="text-light"><small>Create new account</small></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer -->
<footer class="py-5" id="footer-main">
	<div class="container">
		<div class="row align-items-center justify-content-xl-between">
			<div class="col-xl-6">
				<div class="copyright text-center text-xl-left text-muted">
					&copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
				</div>
			</div>
			<div class="col-xl-6">
				<ul class="nav nav-footer justify-content-center justify-content-xl-end">
					<li class="nav-item">
						<a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
					</li>
					<li class="nav-item">
						<a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
					</li>
					<li class="nav-item">
						<a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
					</li>
					<li class="nav-item">
						<a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<?php include('../_partials/bottom-login.php') ?>
