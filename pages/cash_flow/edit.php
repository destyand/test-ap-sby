<?php include('../_partials/top.php') ?>
<?php include('_partials/menu.php') ?>
<?php include('./data-show.php') ?>

<div class="container-fluid mt--6">
<div class="col-xl-12 order-xl-1">
<div class="card">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col-8">
				<h3 class="mb-0">Tambah Kategori </h3>
			</div>
			<div class="col-4 text-right">
				<a href="#!" onclick="window.history.back()" class="btn btn-sm btn-primary">Kembali</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<form action="update.php" method="post">

			<div class="pl-lg-4">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Nama</label>
							<input type="hidden" name="id" value="<?= $datax[0]['id'] ?>" />
							<input type="text" class="form-control" name="nama" value="<?= $datax[0]['nama'] ?>" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Jenis Kategori</label>
							<select class="form-control selectpicker" name="jenis_kategori" required>
								<option value="" selected disabled>- pilih -</option>
								<option value="PEMASUKAN" <?php ($data_warga[0]['jenis_kategori'] === 'PEMASUKAN') ? 'selected' : '' ?>>PEMASUKAN</option>
								<option value="PENGELUARAN" <?php ($data_warga[0]['jenis_kategori'] === 'PENGELUARAN') ? 'selected' : '' ?>>PENGELUARAN</option>
							</select>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Prioritas</label>
							<input type="number" class="form-control" name="prioritas" value="<?= $datax[0]['prioritas'] ?>" required>
						</div>
					</div>
				</div>
			</div>
			
			<button type="submit" class="btn btn-primary btn-lg">
				<i class="glyphicon glyphicon-floppy-save"></i> Simpan
			</button>
		</form>
	</div>
</div>
</div>
</div>

<?php include('../_partials/bottom.php') ?>
