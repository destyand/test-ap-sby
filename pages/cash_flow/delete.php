<?php
include('../../config/koneksi.php');
include('../../pages/_partials/loading-page.php');

// ambil data dari form
$id = htmlspecialchars($_GET['id']);

$query = "DELETE FROM kategori WHERE id = $id";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
	echo "<script language='javascript'>
				setTimeout(() => {
					swal('Sukses','Hapus data ','success').then(() => { window.location.href='../kategori'; });
				}, 1000)
				</script>";
} else {
	echo "<script language='javascript'>
				setTimeout(() => {
					swal('Gagal','Hapus data ','error').then(() => { window.location.href='../kategori'; });
				}, 1000)
				</script>";
}
