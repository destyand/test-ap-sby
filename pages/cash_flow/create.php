<?php include('../_partials/top.php') ?>
<?php include('_partials/menu.php') ?>
<?php 
	// ambil dari database
	$query = "SELECT * FROM kategori";
	
	$hasil = mysqli_query($db, $query);
	
	$data_kategori = array();
	
	while ($row = mysqli_fetch_assoc($hasil)) {
		$data_kategori[] = $row;
	}
?>

<div class="container-fluid mt--6">
<div class="col-xl-12 order-xl-1">
<div class="card">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col-8">
				<h3 class="mb-0">Tambah Kategori </h3>
			</div>
			<div class="col-4 text-right">
				<a href="#!" onclick="window.history.back()" class="btn btn-sm btn-primary">Kembali</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<form action="store.php" method="post">
			
			<div class="pl-lg-4">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">tanggal</label>
							<input type="date" class="form-control" name="tanggal" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">nama</label>
							<input type="text" class="form-control" name="nama" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">nominal</label>
							<input type="number" class="form-control" name="nominal" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Kategori </label>
							<select class="form-control selectlive" id="kategori_id" name="kategori_id" required>
								<option value="" selected disabled>- pilih -</option>
								<?php foreach ($data_kategori as $kat) : ?>
								<option value="<?php echo $kat['id'] ?>">
									<?php echo $kat['nama'] ?>
								</option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			
			<button type="submit" class="btn btn-primary btn-lg">
				<i class="glyphicon glyphicon-floppy-save"></i> Simpan
			</button>
		</form>
	</div>
</div>
</div>
</div>

<?php include('../_partials/bottom.php') ?>
