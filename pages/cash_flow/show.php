<?php include('../_partials/top.php') ?>
<?php include('_partials/menu.php') ?>
<?php include('data-show.php') ?>

<div class="container-fluid mt--6">
<div class="col-xl-12 order-xl-1">
<div class="card">
	<div class="card-header">
		<div class="row align-items-center">
			<div class="col-8">
				<h3 class="mb-0">Tambah Pengguna </h3>
			</div>
			<div class="col-4 text-right">
				<a href="#!" onclick="window.history.back()" class="btn btn-sm btn-primary">Kembali</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<form action="update.php" method="post">
			<h6 class="heading-small text-muted mb-4">A. Data Pribadi</h6>
			<div class="pl-lg-4">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Nama User</label>
							<input type="hidden" name="id_user" value="<?php echo $data_user[0]['id_user'] ?>">
							<input type="text" class="form-control" name="nama_user" value="<?= $data_user[0]['nama_user'] ?>" disabled>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Username</label>
							<input type="text" class="form-control" name="username_user" value="<?= $data_user[0]['username_user'] ?>" disabled>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Keterangan</label>
							<textarea class="form-control" name="keterangan_user" disabled><?= $data_user[0]['keterangan_user'] ?></textarea>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Status</label>
							<select class="form-control selectpicker" name="status_user" disabled>
								<option value="Admin" <?= ($data_user[0]['status_user'] === 'Admin') ? 'selected' : '' ?>>Admin</option>
								<option value="RT" <?= ($data_user[0]['status_user'] === 'RT') ? 'selected' : '' ?>>RT</option>
								<option value="RW" <?= ($data_user[0]['status_user'] === 'RW') ? 'selected' : '' ?>>RW</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<hr class="my-4" />
			<h6 class="heading-small text-muted mb-4">A. Data Alamat</h6>
			<div class="pl-lg-4">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Desa/Kelurahan</label>
							<input type="text" class="form-control" name="desa_kelurahan_user" disabled
							value="<?= $data_user[0]['desa_kelurahan_user'] ?>">
						</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">Kecamatan</label>
									<input
											type="text"
											class="form-control"
											name="kecamatan_user"
											disabled
											value="<?= $data_user[0]['kecamatan_user'] ?>">
							</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">Kabupaten/Kota</label>
									<input
											type="text"
											class="form-control"
											name="kabupaten_kota_user"
											disabled
											value="<?= $data_user[0]['kabupaten_kota_user'] ?>">
							</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">Provinsi</label>
									<input
											type="text"
											class="form-control"
											name="provinsi_user"
											disabled
											value="<?= $data_user[0]['provinsi_user'] ?>">
							</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">Negara</label>
									<input
											type="text"
											class="form-control"
											name="negara_user"
											disabled
											value="<?= $data_user[0]['negara_user'] ?>">
							</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">RT</label>
									<input
											type="text"
											class="form-control"
											name="rt_user"
											disabled
											value="<?= $data_user[0]['rt_user'] ?>">
							</div>
					</div>
					<div class="col-lg-12">
							<div class="form-group">
									<label class="form-control-label">RW</label>
									<input
											type="text"
											class="form-control"
											name="rw_user"
											readonly
											value="<?= $data_user[0]['rw_user'] ?>">
							</div>
					</div>
				</div>
			</div>
			<hr class="my-4" />
			<h6 class="heading-small text-muted mb-4">C. Data Aplikasi</h6>
			<div class="pl-lg-4">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Dibuat</label>
							<input type="text" class="form-control" name="created_at" disabled
							value="<?= date('d F Y', strtotime($data_user[0]['created_at'])) ?>">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Diperbarui</label>
							<input type="text" class="form-control" name="updated_at" disabled
							value="<?= date('d F Y', strtotime($data_user[0]['updated_at'])) ?>">
						</div>
					</div>
			</div>
		</form>
	</div>
</div>
</div>
</div>

<?php include('../_partials/bottom.php') ?>
