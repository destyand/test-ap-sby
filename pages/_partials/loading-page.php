<!doctype html>
<html>
    <head>
       <meta charset="utf-8">
       <!-- <title> CSS Loading Animation </title> -->
       <!-- <link rel="stylesheet" href="cssLoadingAnimation.css"> -->
    </head>
    <body>
        <div class="container">
                <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                </ul>
                <h1>Sedang Diproses</h1>
        </div>
    </body>
</html>
<style rel="stylesheet">
@import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@700&display=swap');
*{
    padding: 0;
    margin: 0;   
    font-family: 'Quicksand', sans-serif;   
}
body{
    background:#5e72e4;
}
.container{
    position: absolute;
    top: 50%;  left:50%;
    transform: translate(-50%,-50%);
    text-align: center;
}
.container ul{
  position: relative;
}
.container ul li{
    list-style: none;
    display: inline-block;
    width: 20px;    height: 20px;
    border-radius: 50%;
    border: 1px solid #fff;
    animation: anime 1s infinite linear;
}
@keyframes anime{
    0%{
        background: #ffc107;
        transform: translateY(0);
    }
    30%{
        background:transparent;
         }
    50%{
        background: #f00;
        transform: translateY(-20px);
    }
    100%{
        background: #ffc107;
        transform: translateY(0);
    }
}
.container ul li:nth-child(1){
    animation-delay: 0s;
}
.container ul li:nth-child(2){
    animation-delay: .1s;
}
.container ul li:nth-child(3){
    animation-delay: .2s;
}
.container ul li:nth-child(4){
    animation-delay: .3s;
}
.container ul li:nth-child(5){
    animation-delay: .4s;
}
.container ul li:nth-child(6){
    animation-delay: .5s;
}

.container h1{
    margin-top: 15px;
}
</style>
<script src="../../assets/extra/vendor/jquery/dist/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
