<?php
error_reporting(0);
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Aplikasi Pendataan Warga RW 08 Tembok Dukuh">
    <meta name="author" content="Pixo Destya Fandy">
    <title>Aplikasi Pendataan Warga RW 08 Tembok Dukuh - Bubutan Surabaya</title>
    <!-- Favicon -->
		<link rel="icon" href="../../assets/img/logo-garuda-hd-34015-16x16.ico" type="image/png">
		<!-- Fonts -->
		<link rel="stylesheet" href="../../assets/cdn/font_css.css">
		<!-- Icons -->
		<link rel="stylesheet" href="../../assets/extra/vendor/nucleo/css/nucleo.css" type="text/css">
		<link rel="stylesheet" href="../../assets/extra/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
		<!-- DataTable CSS -->
		<!-- <link href="../../assets/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
		<link href="../../assets/cdn/bootstrap.css" rel="stylesheet">
		<link href="../../assets/cdn/dataTables.bootstrap4.min.css" rel="stylesheet">
		<link href="../../assets/cdn/responsive.bootstrap4.min.css" rel="stylesheet">
		<!-- Date Range Picker style -->
		<link href="../../assets/css/daterangepicker.css" rel="stylesheet">
		<!-- Argon CSS -->
		<link rel="stylesheet" href="../../assets/extra/css/argon.css?v=1.2.0" type="text/css">
  </head>

  <body>
		<?php include('../_partials/sidebar.php') ?>
		  <!-- Main content -->
			<div class="main-content" id="panel">
				<?php include('../_partials/navbar.php') ?>
