		

</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="../../assets/extra/vendor/jquery/dist/jquery.min.js"></script>
<script src="../../assets/extra/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/extra/vendor/js-cookie/js.cookie.js"></script>
<script src="../../assets/extra/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="../../assets/extra/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<!-- Datatable -->
<!-- <script src="../../assets/js/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="../../assets/js/dataTables.bootstrap.min.js" charset="utf-8"></script> -->
<script src="../../assets/cdn/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="../../assets/cdn/dataTables.bootstrap4.min.js" charset="utf-8"></script>
<script src="../../assets/cdn/dataTables.responsive.min.js" charset="utf-8"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js" charset="utf-8"></script>
<!-- Date Range Picker -->
<script type="text/javascript" src="../../assets/js/moment.min.js"></script>
<script type="text/javascript" src="../../assets/js/daterangepicker.js"></script>
<script src="../../assets/js/bootstrap-select.min.js"></script>
<!-- Argon JS -->
<link href="../../assets/cdn/select2.min.css" rel="stylesheet" />
<script src="../../assets/cdn/select2.min.js"></script>
<script src="../../assets/cdn/sweetalert.min.js"></script>
<script src="../../assets/extra/js/argon.js"></script>
<style>
	.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 12px;
		padding-bottom: 2px;
	}
	.card {
		padding: 15px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
			// $('#datatable').DataTable();
			$('#datatable').dataTable( {
				"language": {
					"paginate": {
						"next": "<i class='fas fa-angle-right'></i>",
						"previous": "<i class='fas fa-angle-left'></i>"
					}
				}
			});
			$('.selectlive').select2({
				liveSearch: true,
				size: 6
			});
			$('.selectpicker').select2();
			$('.datepicker').daterangepicker({
					singleDatePicker: true,
					showDropdowns: true,
					locale: {
						format: 'YYYY-MM-DD',
						monthNames: [
							"Januari",
							"Februari",
							"Maret",
							"April",
							"Mei",
							"Juni",
							"Juli",
							"Agustus",
							"September",
							"Oktober",
							"November",
							"Desember"
						],
						"daysOfWeek": [
							"Mg",
							"Sn",
							"Sl",
							"Rb",
							"Km",
							"Jm",
							"Sb"
						]
					}
			});
			$( "#clear_all_notif" ).on("click", function(e) {
				var idx = '';
				e.preventDefault();
					$.ajax({
						type: 'POST',
						url: '../notifikasi/delete.php',
						data: {
								'idx': idx
						},
						dataType: 'json',
						success: function (data) {
							if(data)
							if(data.status === 'success'){
								window.location.reload()
							} else {
								swal('Gagal', 'Gagal memproses data!' , 'error');
							}
						}
				});
			});
			$( "#read-notification" ).on("click", function(e) {
				var idx = '';
				e.preventDefault();
					$.ajax({
						type: 'POST',
						url: '../notifikasi/update.php',
						data: {
								'idx': idx
						},
						dataType: 'json',
						success: function (data) {
							if(data)
							if(data.status === 'success'){
								// window.location.reload()
								$("#total-notif").remove()
							} else {
								swal('Gagal', 'Gagal memproses data!' , 'error');
							}
						}
				});
			});
	});
</script>
</body>
</html>
