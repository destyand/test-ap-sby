<!-- Argon Scripts -->
  <!-- Core -->
  <script src="../../assets/extra/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../../assets/extra/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/extra/vendor/js-cookie/js.cookie.js"></script>
  <script src="../../assets/extra/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../../assets/extra/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../../assets/extra/js/argon.js?v=1.2.0"></script>
</body>

</html>