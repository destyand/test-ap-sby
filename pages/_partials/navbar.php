<?php include('../notifikasi/notifikasi.php') ?>
<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
	<div class="container-fluid">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Search form -->
			<!-- <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
				<div class="form-group mb-0">
					<div class="input-group input-group-alternative input-group-merge">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-search"></i></span>
						</div>
						<input class="form-control" placeholder="Search" type="text">
					</div>
				</div>
				<button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</form> -->
			<span class="mb-0 text-sm font-weight-bold" style="color: white;">ANDA LOGIN SEBAGAI <?= strtoupper($_SESSION['user']['status_user']) ?> <?= strtoupper($_SESSION['user']['rt_user']) ?></span>
			<!-- Navbar links -->
			<ul class="navbar-nav align-items-center  ml-md-auto ">
				<li class="nav-item d-xl-none">
					<!-- Sidenav toggler -->
					<div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
						<div class="sidenav-toggler-inner">
							<i class="sidenav-toggler-line"></i>
							<i class="sidenav-toggler-line"></i>
							<i class="sidenav-toggler-line"></i>
						</div>
					</div>
				</li>
				<li class="nav-item d-sm-none">
					<a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
						<i class="ni ni-zoom-split-in"></i>
					</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="read-notification">
						<i class="ni ni-bell-55">
						<?php if((int)$count_log['total'] > 0): ?>
							<span class="badge badge-pill badge-success" id="total-notif"><?= $count_log['total'] ?></span>
						<?php endif; ?>
						</i>
					</a>
					<div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden" style="overflow: scroll !important; height: 500px;">
						<!-- Dropdown header -->
						<div class="px-3 py-3">
							<h6 class="text-sm text-muted m-0">Anda Punya <strong class="text-primary"><?= $count_log['total'] ?></strong> notifikasi.</h6>
						</div>
						<!-- List group -->
						<div class="list-group list-group-flush">
							<?php foreach ($data_log as $data) : ?>
							<?php $linked_to = '' ?>
							<?php if ($data['link_to'] === '../warga'){ 
								$linked_to = '../warga/show.php?id_warga='.$data['last_id'];
							} elseif ($data['link_to'] === '../surat-pengantar'){
								$linked_to = '../surat-pengantar/show.php?id_pengantar='.$data['last_id'];
							} elseif ($data['link_to'] === '../kader'){
								$linked_to = '../kader/show.php?id_kader='.$data['last_id'];
							}
							?>
							<a href="<?= $linked_to ?>" id="to_detail_notif" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
									<div class="col-auto">
										<!-- Avatar -->
										<!-- <img alt="Image placeholder" src="../../assets/extra/img/theme/team-1.jpg" class="avatar rounded-circle"> -->
									</div>
									<div class="col ml--2">
										<div class="d-flex justify-content-between align-items-center">
											<div>
												<h4 class="mb-0 text-sm"><?= $data['nama_user'] ?> (RT: <?= $data['rt_user'] ?>)</h4>
											</div>
											<div class="text-right text-muted">
												<small><?= date('d F Y', strtotime($data['created_at'])) ?></small>
											</div>
										</div>
										<p class="text-sm mb-0"><?= $data['log_name'] ?></p>
									</div>
								</div>
							</a>
							<?php endforeach; ?>
						</div>
						<!-- View all -->
						<?php if ($_SESSION['user']['status_user'] === 'RW' || $_SESSION['user']['status_user'] === 'Admin'): ?>
							<?php if((int)$count_all_log['total'] > 0): ?>
								<a href="#!" id="clear_all_notif" class="dropdown-item text-center text-primary font-weight-bold py-3">Bersihkan Semua</a>
							<?php endif; ?>
							</div>
						<?php endif; ?>
				</li>
			</ul>
			<ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
				<li class="nav-item dropdown">
					<a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<div class="media align-items-center">
							<span class="avatar avatar-sm rounded-circle">
								<img alt="Image placeholder" src="../../assets/img/logo-garuda-hd-34015.png">
							</span>
							<div class="media-body  ml-2  d-none d-lg-block">
								<span class="mb-0 text-sm  font-weight-bold"><?= strtoupper($_SESSION['user']['username_user']) ?></span>
							</div>
						</div>
					</a>
					<div class="dropdown-menu  dropdown-menu-right ">
						<div class="dropdown-header noti-title">
							<h6 class="text-overflow m-0">Welcome!</h6>
						</div>
						<div class="dropdown-divider"></div>
						<a href="../login/logout.php" class="dropdown-item">
							<i class="ni ni-user-run"></i>
							<span>Logout</span>
						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>